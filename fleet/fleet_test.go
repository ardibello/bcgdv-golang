// Testing is done with simple comparisons, and no assertion library is used
// in order to make it simpler to run and not to rely on external dependencies.

package fleet

import "testing"

const TestFailed = "Test Failed"

func TestGetNumberOfEngineers(t *testing.T) {
	if GetNumberOfEngineers([]int{15, 10}, 12, 5) != 3 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{11, 15, 13}, 9, 5) != 7 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{5, 7, 11}, 4, 2) != 11 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{5, 7, 11}, 4, 3) != 7 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{15, 4, 5}, 8, 7) != 3 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{16, 8, 6}, 13, 7) != 4 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{17, 15, 1}, 1, 3) != 11 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{5, 16, 9}, 20, 9) != 2 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{15, 11, 9}, 10, 7) != 5 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{21, 15, 1}, 3, 9) != 5 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{10}, 1, 1) != 9 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{10}, 2, 3) != 3 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{8, 9, 10, 14, 15}, 9, 5) != 10 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{0, 0, 0}, 4, 2) != 0 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{0, 3, 0}, 4, 2) != 0 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{4, 0, 0}, 4, 2) != 0 {
		t.Error(TestFailed)
	}
	if GetNumberOfEngineers([]int{0, 0, 5}, 4, 2) != 1 {
		t.Error(TestFailed)
	}
}
