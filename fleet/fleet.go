package fleet

import (
	"math"
)

// GetNumberOfEngineers inds minimum number of engineers needed to help the FM.
func GetNumberOfEngineers(scooters []int, C int, P int) int {
	// engineersNeededInManagerDistrict is an array used to store the number of engineers needed
	// in one district (will be used inside the loop when all districts will be iterated).
	// It contains 2 elements. The first one shows the number of engineers needed in the
	// district if the Fleet Manager will NOT be in that district. The second element
	// shows the number of engineers needed if the Fleet Manager will be in that district.
	// Initially the FM district will be the first district.
	var engineersNeededInManagerDistrict [2]int

	engineersNeededInManagerDistrict[0] = int(math.Ceil(float64(scooters[0]) / float64(P)))
	if scooters[0] > C {
		engineersNeededInManagerDistrict[1] = int(math.Ceil(float64(scooters[0]-C) / float64(P)))
	} else {
		engineersNeededInManagerDistrict[1] = 0
	}

	// A helper array used for possible new district that the FM can be located in.
	// If this new district presents the opportunity to use less FEs, than this
	// array substitutes the engineersNeededInManagerDistrict bringing a new combination.
	var engineersNeededInManagerDistrictTemp [2]int

	// A counter to keep track of engineers needed for all the districts.
	// Recalculated new districts are analysed.
	totalEngineersNeeded := engineersNeededInManagerDistrict[1]

	// A loop going through each district (starting from the second one since
	// calculations for first districts are already done). If a better combination
	// is spotted than the above variables are recalculated.
	for i := 1; i < len(scooters); i++ {
		engineersNeededInManagerDistrictTemp[0] = int(math.Ceil(float64(scooters[i]) / float64(P)))
		if scooters[i] > C {
			engineersNeededInManagerDistrictTemp[1] = int(math.Ceil(float64(scooters[i]-C) / float64(P)))
		} else {
			engineersNeededInManagerDistrictTemp[1] = 0
		}

		// Check if the district being analysed will save engineers
		// if the FM were to be located there.
		if engineersNeededInManagerDistrictTemp[0]-engineersNeededInManagerDistrictTemp[1] > engineersNeededInManagerDistrict[0]-engineersNeededInManagerDistrict[1] {
			// Fm will be at current district, remake calculations.
			totalEngineersNeeded =
				totalEngineersNeeded -
					engineersNeededInManagerDistrict[1] +
					engineersNeededInManagerDistrict[0] +
					engineersNeededInManagerDistrictTemp[1]

			engineersNeededInManagerDistrict[0] =
				engineersNeededInManagerDistrictTemp[0]
			engineersNeededInManagerDistrict[1] =
				engineersNeededInManagerDistrictTemp[1]
		} else {
			// Fm will continue to be at the district he is at.
			// Add number of engineers needed for this district.
			totalEngineersNeeded += engineersNeededInManagerDistrictTemp[0]
		}
	}

	return totalEngineersNeeded
}
