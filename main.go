package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/ardibello/bcgdv-golang/fleet"
)

// Request is a struct representing a JSON request body.
type Request struct {
	Scooters []int `json:"scooters"`
	C        int
	P        int
}

// Response is a struct representing a JSON response body.
type Response struct {
	FleetEngineers int `json:"fleet_engineers"`
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Stores HTTP request body values.
		var request Request

		// Decode HTTP data and assign values to request.
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&request)
		if err != nil {
			panic(err)
		}

		// Get minimum number of fleet engineers needed.
		fleetEngineers := fleet.GetNumberOfEngineers(request.Scooters, request.C, request.P)

		// Construct JSON and return HTTP response.
		response := Response{fleetEngineers}
		jsonData, err := json.Marshal(response)
		if err != nil {
			fmt.Print("An error occurred")
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonData)
	})

	log.Fatal(http.ListenAndServe(":3000", nil))
}
