# BSGDV Challenge - Golang
### How to Run

[Go](https://golang.org/) is required to run this application.

`go get` this repo:
```sh
$ go get bitbucket.org/ardibello/bcgdv-golang
```
Open terminal and navigate to the repository/project root (must be in `GOPATH/src/bitbucket.org/ardibello/bcgdv-golang`):
```sh
$ cd bcgdv-node
```
Run the server:
```sh
$ go run main.go
```
Make an HTTP POST request to `http://localhost:3000` with a JSON body of the following format:
```json
{
 "scooters": [21, 15, 1],
 "C": 3,
 "P": 9
}
```

### Run Tests
Run the following command in terminal from the project root:
```sh
$ go test ./...
```